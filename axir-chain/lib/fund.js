/** 
* Get Fee Wallet 
*/
async function getFeeWallet() {

    // Get the asset registry for the asset.
    return await getAssetRegistry('org.axir.chain.Wallet')
                    .then((assetRegistry) => assetRegistry.get('Master-Fee-Wallet'));

}
/**
* Get Estimated Fee
*/
async function getEstimatedFee(amount) {

    // 0.5% of the fee + 0.001 as Base Fee
    let feeMultiplyer = 0.005;
    let baseFee = 0.001;

    let fee = parseFloat(baseFee + parseFloat(parseFloat(amount) * feeMultiplyer));

    return fee;
}

/**
 * Credit transaction processor function.
 * @param {org.axir.chain.CreditProcessor} credit The credit instance for wallet.
 * @transaction
 */
async function creditProcessor(credit) {

    // Save the old value of the asset.
    let oldValue = credit.wallet.balance;


    // Update the asset with the new value.
    credit.wallet.balance = parseFloat(oldValue) + parseFloat(credit.newValue);


    // Get the asset registry for the asset.
    let assetRegistry = await getAssetRegistry('org.axir.chain.Wallet');


    // emit a notification that a credit transaction has occurred
    const creditNotification = getFactory().newEvent('org.axir.chain', 'CreditNotification');
    creditNotification.wallet = credit.wallet;
    creditNotification.amount = credit.newValue;
    emit(creditNotification);



    // Update the asset in the asset registry.
    assetRegistry.update(credit.wallet);




}

/**
 * Debit transaction processor function.
 * @param {org.axir.chain.DebitProcessor} debit The debit instance for wallet.
 * @transaction
 */
async function debitProcessor(debit) {

    // Save the old value of the asset.
    let oldValue = debit.wallet.balance;

    if (oldValue == 0 || oldValue < debit.newValue) {

        throw new Error('Insufficient Balance');

    } else {

        // Update the asset with the new value.
        debit.wallet.balance = parseFloat(oldValue) - parseFloat(debit.newValue);

    }

    // Get the asset registry for the asset.
    let assetRegistry = await getAssetRegistry('org.axir.chain.Wallet');

    // emit a notification that a debit transaction has occurred
    const debitNotification = getFactory().newEvent('org.axir.chain', 'DebitNotification');
    debitNotification.wallet = debit.wallet;
    debitNotification.amount = debit.newValue;
    emit(debitNotification);

    // Update the asset in the asset registry.
    assetRegistry.update(debit.wallet);


}




/**
 * Transfer processor function.
 * @param {org.axir.chain.TransferProcessor} transfer 
 * @transaction
 */
async function transferProcessor(transfer) {



    // Save the old value of the asset.

    let from_wallet_oldValue = transfer.wallet_from.balance;
    let to_wallet_oldValue = transfer.wallet_to.balance;


    let feeWallet = await getFeeWallet().then((feeWallet) => { return feeWallet });

    let fee = parseFloat(getEstimatedFee(transfer.newValue));

    /// define credit and debit
    let debitValue = parseFloat(transfer.newValue) + parseFloat(fee);


    if (from_wallet_oldValue == 0 || from_wallet_oldValue < debitValue) {

        throw new Error('Insufficient Balance');

    } else {

        let credit = { wallet: transfer.wallet_to, newValue: transfer.newValue }
        let debit = { wallet: transfer.wallet_from, newValue: debitValue }
        let feeCredit = { wallet: feeWallet, newValue: fee }

        await debitProcessor(debit)
        await creditProcessor(credit)
        await creditProcessor(feeCredit)

        // emit a notification that a debit transaction has occurred
        const transferNotification = getFactory().newEvent('org.axir.chain', 'TransferNotification');
        transferNotification.wallet_from = transfer.wallet_from;
        transferNotification.wallet_to = transfer.wallet_to;
        transferNotification.amount = transfer.newValue;
        transferNotification.fee = fee;
        emit(transferNotification);

    }

}
/**
 * Send Many Transfer processor function.
 * @param {org.axir.chain.SendManyProcessor} send 
 * @transaction
 */
async function sendManyProcessor(send) {

    let receivers = send.receivers;
    let totalSendAmount = 0;

    for (i = 0; i < receivers.length; i++) {  //loop through the array
        totalSendAmount += receivers[i].amount;  //Do the math!
    }

    console.log(totalSendAmount);


    let feeWallet = await getFeeWallet().then((feeWallet) => { return feeWallet });

    let fee = parseFloat(getEstimatedFee(totalSendAmount));

    // define credit and debit
    let debitValue = parseFloat(totalSendAmount) + parseFloat(fee);


    await debitProcessor({ wallet: send.sender, newValue: debitValue })
    await creditProcessor({ wallet: feeWallet, newValue: fee })

    for (i = 0; i < receivers.length; i++) {  //loop through the array
        await creditProcessor({
            wallet: receivers[i].wallet,
            newValue: receivers[i].amount
        })
    }
} 