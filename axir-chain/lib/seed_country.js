/**
 * Function to seed the countries
 * @param {org.axir.chain.SeedCountry} c
 * @transaction
 */

async function SeedCountry(c) {

    const country = [
        {
            "id": 1,
            "name": "AFGHANISTAN",
            "short_name": "AF",
            "iso_code\t": "AFG",
            "tel_prefix": "93",
            "status": "active"
        },
        {
            "id": 2,
            "name": "ALBANIA",
            "short_name": "AL",
            "iso_code\t": "ALB",
            "tel_prefix": "355",
            "status": "active"
        },
        {
            "id": 3,
            "name": "ALGERIA",
            "short_name": "DZ",
            "iso_code\t": "DZA",
            "tel_prefix": "213",
            "status": "inactive"
        },
        {
            "id": 4,
            "name": "AMERICAN SAMOA",
            "short_name": "AS",
            "iso_code\t": "ASM",
            "tel_prefix": "1684",
            "status": "inactive"
        },
        {
            "id": 5,
            "name": "ANDORRA",
            "short_name": "AD",
            "iso_code\t": "AND",
            "tel_prefix": "376",
            "status": "inactive"
        },
        {
            "id": 6,
            "name": "ANGOLA",
            "short_name": "AO",
            "iso_code\t": "AGO",
            "tel_prefix": "244",
            "status": "inactive"
        },
        {
            "id": 7,
            "name": "ANGUILLA",
            "short_name": "AI",
            "iso_code\t": "AIA",
            "tel_prefix": "1264",
            "status": "inactive"
        },
        {
            "id": 8,
            "name": "ANTARCTICA",
            "short_name": "AQ",
            "iso_code\t": "ANT",
            "tel_prefix": "11",
            "status": "inactive"
        },
        {
            "id": 9,
            "name": "ANTIGUA AND BARBUDA",
            "short_name": "AG",
            "iso_code\t": "ATG",
            "tel_prefix": "1268",
            "status": "inactive"
        },
        {
            "id": 10,
            "name": "ARGENTINA",
            "short_name": "AR",
            "iso_code\t": "ARG",
            "tel_prefix": "54",
            "status": "inactive"
        },
        {
            "id": 11,
            "name": "ARMENIA",
            "short_name": "AM",
            "iso_code\t": "ARM",
            "tel_prefix": "374",
            "status": "inactive"
        },
        {
            "id": 12,
            "name": "ARUBA",
            "short_name": "AW",
            "iso_code\t": "ABW",
            "tel_prefix": "297",
            "status": "inactive"
        },
        {
            "id": 13,
            "name": "AUSTRALIA",
            "short_name": "AU",
            "iso_code\t": "AUS",
            "tel_prefix": "61",
            "status": "inactive"
        },
        {
            "id": 14,
            "name": "AUSTRIA",
            "short_name": "AT",
            "iso_code\t": "AUT",
            "tel_prefix": "43",
            "status": "inactive"
        },
        {
            "id": 15,
            "name": "AZERBAIJAN",
            "short_name": "AZ",
            "iso_code\t": "AZE",
            "tel_prefix": "994",
            "status": "inactive"
        },
        {
            "id": 16,
            "name": "BAHAMAS",
            "short_name": "BS",
            "iso_code\t": "BHS",
            "tel_prefix": "1242",
            "status": "inactive"
        },
        {
            "id": 17,
            "name": "BAHRAIN",
            "short_name": "BH",
            "iso_code\t": "BHR",
            "tel_prefix": "973",
            "status": "inactive"
        },
        {
            "id": 18,
            "name": "BANGLADESH",
            "short_name": "BD",
            "iso_code\t": "BGD",
            "tel_prefix": "880",
            "status": "inactive"
        },
        {
            "id": 19,
            "name": "BARBADOS",
            "short_name": "BB",
            "iso_code\t": "BRB",
            "tel_prefix": "1246",
            "status": "inactive"
        },
        {
            "id": 20,
            "name": "BELARUS",
            "short_name": "BY",
            "iso_code\t": "BLR",
            "tel_prefix": "375",
            "status": "inactive"
        },
        {
            "id": 21,
            "name": "BELGIUM",
            "short_name": "BE",
            "iso_code\t": "BEL",
            "tel_prefix": "32",
            "status": "inactive"
        },
        {
            "id": 22,
            "name": "BELIZE",
            "short_name": "BZ",
            "iso_code\t": "BLZ",
            "tel_prefix": "501",
            "status": "inactive"
        },
        {
            "id": 23,
            "name": "BENIN",
            "short_name": "BJ",
            "iso_code\t": "BEN",
            "tel_prefix": "229",
            "status": "inactive"
        },
        {
            "id": 24,
            "name": "BERMUDA",
            "short_name": "BM",
            "iso_code\t": "BMU",
            "tel_prefix": "1441",
            "status": "inactive"
        },
        {
            "id": 25,
            "name": "BHUTAN",
            "short_name": "BT",
            "iso_code\t": "BTN",
            "tel_prefix": "975",
            "status": "inactive"
        },
        {
            "id": 26,
            "name": "BOLIVIA",
            "short_name": "BO",
            "iso_code\t": "BOL",
            "tel_prefix": "591",
            "status": "inactive"
        },
        {
            "id": 27,
            "name": "BOSNIA AND HERZEGOVINA",
            "short_name": "BA",
            "iso_code\t": "BIH",
            "tel_prefix": "387",
            "status": "inactive"
        },
        {
            "id": 28,
            "name": "BOTSWANA",
            "short_name": "BW",
            "iso_code\t": "BWA",
            "tel_prefix": "267",
            "status": "inactive"
        },
        {
            "id": 29,
            "name": "BOUVET ISLAND",
            "short_name": "BV",
            "iso_code\t": "BI",
            "tel_prefix": "33",
            "status": "inactive"
        },
        {
            "id": 30,
            "name": "BRAZIL",
            "short_name": "BR",
            "iso_code\t": "BRA",
            "tel_prefix": "55",
            "status": "inactive"
        },
        {
            "id": 31,
            "name": "BRITISH INDIAN OCEAN TERRITORY",
            "short_name": "IO",
            "iso_code\t": "IO",
            "tel_prefix": "246",
            "status": "inactive"
        },
        {
            "id": 32,
            "name": "BRUNEI DARUSSALAM",
            "short_name": "BN",
            "iso_code\t": "BRN",
            "tel_prefix": "673",
            "status": "inactive"
        },
        {
            "id": 33,
            "name": "BULGARIA",
            "short_name": "BG",
            "iso_code\t": "BGR",
            "tel_prefix": "359",
            "status": "inactive"
        },
        {
            "id": 34,
            "name": "BURKINA FASO",
            "short_name": "BF",
            "iso_code\t": "BFA",
            "tel_prefix": "226",
            "status": "inactive"
        },
        {
            "id": 35,
            "name": "BURUNDI",
            "short_name": "BI",
            "iso_code\t": "BDI",
            "tel_prefix": "257",
            "status": "inactive"
        },
        {
            "id": 36,
            "name": "CAMBODIA",
            "short_name": "KH",
            "iso_code\t": "KHM",
            "tel_prefix": "855",
            "status": "inactive"
        },
        {
            "id": 37,
            "name": "CAMEROON",
            "short_name": "CM",
            "iso_code\t": "CMR",
            "tel_prefix": "237",
            "status": "inactive"
        },
        {
            "id": 38,
            "name": "CANADA",
            "short_name": "CA",
            "iso_code\t": "CAN",
            "tel_prefix": "1",
            "status": "inactive"
        },
        {
            "id": 39,
            "name": "CAPE VERDE",
            "short_name": "CV",
            "iso_code\t": "CPV",
            "tel_prefix": "238",
            "status": "inactive"
        },
        {
            "id": 40,
            "name": "CAYMAN ISLANDS",
            "short_name": "KY",
            "iso_code\t": "CYM",
            "tel_prefix": "1345",
            "status": "inactive"
        },
        {
            "id": 41,
            "name": "CENTRAL AFRICAN REPUBLIC",
            "short_name": "CF",
            "iso_code\t": "CAF",
            "tel_prefix": "236",
            "status": "inactive"
        },
        {
            "id": 42,
            "name": "CHAD",
            "short_name": "TD",
            "iso_code\t": "TCD",
            "tel_prefix": "235",
            "status": "inactive"
        },
        {
            "id": 43,
            "name": "CHILE",
            "short_name": "CL",
            "iso_code\t": "CHL",
            "tel_prefix": "56",
            "status": "inactive"
        },
        {
            "id": 44,
            "name": "CHINA",
            "short_name": "CN",
            "iso_code\t": "CHN",
            "tel_prefix": "86",
            "status": "inactive"
        },
        {
            "id": 45,
            "name": "CHRISTMAS ISLAND",
            "short_name": "CX",
            "iso_code\t": "CX",
            "tel_prefix": "61",
            "status": "inactive"
        },
        {
            "id": 46,
            "name": "COCOS [ KEELING],ISLANDS",
            "short_name": "CC",
            "iso_code\t": "CC",
            "tel_prefix": "672",
            "status": "inactive"
        },
        {
            "id": 47,
            "name": "COLOMBIA",
            "short_name": "CO",
            "iso_code\t": "COL",
            "tel_prefix": "57",
            "status": "inactive"
        },
        {
            "id": 48,
            "name": "COMOROS",
            "short_name": "KM",
            "iso_code\t": "COM",
            "tel_prefix": "269",
            "status": "inactive"
        },
        {
            "id": 49,
            "name": "CONGO",
            "short_name": "CG",
            "iso_code\t": "COG",
            "tel_prefix": "242",
            "status": "inactive"
        },
        {
            "id": 50,
            "name": "CONGO, THE DEMOCRATIC REPUBLIC OF THE",
            "short_name": "CD",
            "iso_code\t": "COD",
            "tel_prefix": "242",
            "status": "inactive"
        },
        {
            "id": 51,
            "name": "COOK ISLANDS",
            "short_name": "CK",
            "iso_code\t": "COK",
            "tel_prefix": "682",
            "status": "inactive"
        },
        {
            "id": 52,
            "name": "COSTA RICA",
            "short_name": "CR",
            "iso_code\t": "CRI",
            "tel_prefix": "506",
            "status": "inactive"
        },
        {
            "id": 53,
            "name": "COTE DIVOIRE",
            "short_name": "CI",
            "iso_code\t": "CIV",
            "tel_prefix": "225",
            "status": "inactive"
        },
        {
            "id": 54,
            "name": "CROATIA",
            "short_name": "HR",
            "iso_code\t": "HRV",
            "tel_prefix": "385",
            "status": "inactive"
        },
        {
            "id": 55,
            "name": "CUBA",
            "short_name": "CU",
            "iso_code\t": "CUB",
            "tel_prefix": "53",
            "status": "inactive"
        },
        {
            "id": 56,
            "name": "CYPRUS",
            "short_name": "CY",
            "iso_code\t": "CYP",
            "tel_prefix": "357",
            "status": "inactive"
        },
        {
            "id": 57,
            "name": "CZECH REPUBLIC",
            "short_name": "CZ",
            "iso_code\t": "CZE",
            "tel_prefix": "420",
            "status": "inactive"
        },
        {
            "id": 58,
            "name": "DENMARK",
            "short_name": "DK",
            "iso_code\t": "DNK",
            "tel_prefix": "45",
            "status": "inactive"
        },
        {
            "id": 59,
            "name": "DJIBOUTI",
            "short_name": "DJ",
            "iso_code\t": "DJI",
            "tel_prefix": "253",
            "status": "inactive"
        },
        {
            "id": 60,
            "name": "DOMINICA",
            "short_name": "DM",
            "iso_code\t": "DMA",
            "tel_prefix": "1767",
            "status": "inactive"
        },
        {
            "id": 61,
            "name": "DOMINICAN REPUBLIC",
            "short_name": "DO",
            "iso_code\t": "DOM",
            "tel_prefix": "1809",
            "status": "inactive"
        },
        {
            "id": 62,
            "name": "ECUADOR",
            "short_name": "EC",
            "iso_code\t": "ECU",
            "tel_prefix": "593",
            "status": "inactive"
        },
        {
            "id": 63,
            "name": "EGYPT",
            "short_name": "EG",
            "iso_code\t": "EGY",
            "tel_prefix": "20",
            "status": "inactive"
        },
        {
            "id": 64,
            "name": "EL SALVADOR",
            "short_name": "SV",
            "iso_code\t": "SLV",
            "tel_prefix": "503",
            "status": "inactive"
        },
        {
            "id": 65,
            "name": "EQUATORIAL GUINEA",
            "short_name": "GQ",
            "iso_code\t": "GNQ",
            "tel_prefix": "240",
            "status": "inactive"
        },
        {
            "id": 66,
            "name": "ERITREA",
            "short_name": "ER",
            "iso_code\t": "ERI",
            "tel_prefix": "291",
            "status": "inactive"
        },
        {
            "id": 67,
            "name": "ESTONIA",
            "short_name": "EE",
            "iso_code\t": "EST",
            "tel_prefix": "372",
            "status": "inactive"
        },
        {
            "id": 68,
            "name": "ETHIOPIA",
            "short_name": "ET",
            "iso_code\t": "ETH",
            "tel_prefix": "251",
            "status": "inactive"
        },
        {
            "id": 69,
            "name": "FALKLAND ISLANDS -MALVINAS",
            "short_name": "FK",
            "iso_code\t": "FLK",
            "tel_prefix": "500",
            "status": "inactive"
        },
        {
            "id": 70,
            "name": "FAROE ISLANDS",
            "short_name": "FO",
            "iso_code\t": "FRO",
            "tel_prefix": "298",
            "status": "inactive"
        },
        {
            "id": 71,
            "name": "FIJI",
            "short_name": "FJ",
            "iso_code\t": "FJI",
            "tel_prefix": "679",
            "status": "inactive"
        },
        {
            "id": 72,
            "name": "FINLAND",
            "short_name": "FI",
            "iso_code\t": "FIN",
            "tel_prefix": "358",
            "status": "inactive"
        },
        {
            "id": 73,
            "name": "FRANCE",
            "short_name": "FR",
            "iso_code\t": "FRA",
            "tel_prefix": "33",
            "status": "inactive"
        },
        {
            "id": 74,
            "name": "FRENCH GUIANA",
            "short_name": "GF",
            "iso_code\t": "GUF",
            "tel_prefix": "594",
            "status": "inactive"
        },
        {
            "id": 75,
            "name": "FRENCH POLYNESIA",
            "short_name": "PF",
            "iso_code\t": "PYF",
            "tel_prefix": "689",
            "status": "inactive"
        },
        {
            "id": 76,
            "name": "FRENCH SOUTHERN TERRITORIES",
            "short_name": "TF",
            "iso_code\t": "TF",
            "tel_prefix": "11",
            "status": "inactive"
        },
        {
            "id": 77,
            "name": "GABON",
            "short_name": "GA",
            "iso_code\t": "GAB",
            "tel_prefix": "241",
            "status": "inactive"
        },
        {
            "id": 78,
            "name": "GAMBIA",
            "short_name": "GM",
            "iso_code\t": "GMB",
            "tel_prefix": "220",
            "status": "inactive"
        },
        {
            "id": 79,
            "name": "GEORGIA",
            "short_name": "GE",
            "iso_code\t": "GEO",
            "tel_prefix": "995",
            "status": "inactive"
        },
        {
            "id": 80,
            "name": "GERMANY",
            "short_name": "DE",
            "iso_code\t": "DEU",
            "tel_prefix": "49",
            "status": "inactive"
        },
        {
            "id": 81,
            "name": "GHANA",
            "short_name": "GH",
            "iso_code\t": "GHA",
            "tel_prefix": "233",
            "status": "inactive"
        },
        {
            "id": 82,
            "name": "GIBRALTAR",
            "short_name": "GI",
            "iso_code\t": "GIB",
            "tel_prefix": "350",
            "status": "inactive"
        },
        {
            "id": 83,
            "name": "GREECE",
            "short_name": "GR",
            "iso_code\t": "GRC",
            "tel_prefix": "30",
            "status": "inactive"
        },
        {
            "id": 84,
            "name": "GREENLAND",
            "short_name": "GL",
            "iso_code\t": "GRL",
            "tel_prefix": "299",
            "status": "inactive"
        },
        {
            "id": 85,
            "name": "GRENADA",
            "short_name": "GD",
            "iso_code\t": "GRD",
            "tel_prefix": "1473",
            "status": "inactive"
        },
        {
            "id": 86,
            "name": "GUADELOUPE",
            "short_name": "GP",
            "iso_code\t": "GLP",
            "tel_prefix": "590",
            "status": "inactive"
        },
        {
            "id": 87,
            "name": "GUAM",
            "short_name": "GU",
            "iso_code\t": "GUM",
            "tel_prefix": "1671",
            "status": "inactive"
        },
        {
            "id": 88,
            "name": "GUATEMALA",
            "short_name": "GT",
            "iso_code\t": "GTM",
            "tel_prefix": "502",
            "status": "inactive"
        },
        {
            "id": 89,
            "name": "GUINEA",
            "short_name": "GN",
            "iso_code\t": "GIN",
            "tel_prefix": "224",
            "status": "inactive"
        },
        {
            "id": 90,
            "name": "GUINEA-BISSAU",
            "short_name": "GW",
            "iso_code\t": "GNB",
            "tel_prefix": "245",
            "status": "inactive"
        },
        {
            "id": 91,
            "name": "GUYANA",
            "short_name": "GY",
            "iso_code\t": "GUY",
            "tel_prefix": "592",
            "status": "inactive"
        },
        {
            "id": 92,
            "name": "HAITI",
            "short_name": "HT",
            "iso_code\t": "HTI",
            "tel_prefix": "509",
            "status": "inactive"
        },
        {
            "id": 93,
            "name": "HEARD ISLAND AND MCDONALD ISLANDS",
            "short_name": "HM",
            "iso_code\t": "HM",
            "tel_prefix": "11",
            "status": "inactive"
        },
        {
            "id": 94,
            "name": "HOLY SEE-VATICAN CITY STATE",
            "short_name": "VA",
            "iso_code\t": "VAT",
            "tel_prefix": "39",
            "status": "inactive"
        },
        {
            "id": 95,
            "name": "HONDURAS",
            "short_name": "HN",
            "iso_code\t": "HND",
            "tel_prefix": "504",
            "status": "inactive"
        },
        {
            "id": 96,
            "name": "HONG KONG",
            "short_name": "HK",
            "iso_code\t": "HKG",
            "tel_prefix": "852",
            "status": "inactive"
        },
        {
            "id": 97,
            "name": "HUNGARY",
            "short_name": "HU",
            "iso_code\t": "HUN",
            "tel_prefix": "36",
            "status": "inactive"
        },
        {
            "id": 98,
            "name": "ICELAND",
            "short_name": "IS",
            "iso_code\t": "ISL",
            "tel_prefix": "354",
            "status": "inactive"
        },
        {
            "id": 99,
            "name": "INDIA",
            "short_name": "IN",
            "iso_code\t": "IND",
            "tel_prefix": "91",
            "status": "inactive"
        },
        {
            "id": 100,
            "name": "INDONESIA",
            "short_name": "ID",
            "iso_code\t": "IDN",
            "tel_prefix": "62",
            "status": "inactive"
        },
        {
            "id": 101,
            "name": "IRAN, ISLAMIC REPUBLIC OF",
            "short_name": "IR",
            "iso_code\t": "IRN",
            "tel_prefix": "98",
            "status": "inactive"
        },
        {
            "id": 102,
            "name": "IRAQ",
            "short_name": "IQ",
            "iso_code\t": "IRQ",
            "tel_prefix": "964",
            "status": "inactive"
        },
        {
            "id": 103,
            "name": "IRELAND",
            "short_name": "IE",
            "iso_code\t": "IRL",
            "tel_prefix": "353",
            "status": "inactive"
        },
        {
            "id": 104,
            "name": "ISRAEL",
            "short_name": "IL",
            "iso_code\t": "ISR",
            "tel_prefix": "972",
            "status": "inactive"
        },
        {
            "id": 105,
            "name": "ITALY",
            "short_name": "IT",
            "iso_code\t": "ITA",
            "tel_prefix": "39",
            "status": "inactive"
        },
        {
            "id": 106,
            "name": "JAMAICA",
            "short_name": "JM",
            "iso_code\t": "JAM",
            "tel_prefix": "1876",
            "status": "inactive"
        },
        {
            "id": 107,
            "name": "JAPAN",
            "short_name": "JP",
            "iso_code\t": "JPN",
            "tel_prefix": "81",
            "status": "inactive"
        },
        {
            "id": 108,
            "name": "JORDAN",
            "short_name": "JO",
            "iso_code\t": "JOR",
            "tel_prefix": "962",
            "status": "inactive"
        },
        {
            "id": 109,
            "name": "KAZAKHSTAN",
            "short_name": "KZ",
            "iso_code\t": "KAZ",
            "tel_prefix": "7",
            "status": "inactive"
        },
        {
            "id": 110,
            "name": "KENYA",
            "short_name": "KE",
            "iso_code\t": "KEN",
            "tel_prefix": "254",
            "status": "inactive"
        },
        {
            "id": 111,
            "name": "KIRIBATI",
            "short_name": "KI",
            "iso_code\t": "KIR",
            "tel_prefix": "686",
            "status": "inactive"
        },
        {
            "id": 112,
            "name": "KOREA-DEMOCRATIC PEOPLES REPUBLIC OF",
            "short_name": "KP",
            "iso_code\t": "PRK",
            "tel_prefix": "850",
            "status": "inactive"
        },
        {
            "id": 113,
            "name": "KOREA, REPUBLIC OF",
            "short_name": "KR",
            "iso_code\t": "KOR",
            "tel_prefix": "82",
            "status": "inactive"
        },
        {
            "id": 114,
            "name": "KUWAIT",
            "short_name": "KW",
            "iso_code\t": "KWT",
            "tel_prefix": "965",
            "status": "inactive"
        },
        {
            "id": 115,
            "name": "KYRGYZSTAN",
            "short_name": "KG",
            "iso_code\t": "KGZ",
            "tel_prefix": "996",
            "status": "inactive"
        },
        {
            "id": 116,
            "name": "LAO PEOPLES DEMOCRATIC REPUBLIC",
            "short_name": "LA",
            "iso_code\t": "LAO",
            "tel_prefix": "856",
            "status": "inactive"
        },
        {
            "id": 117,
            "name": "LATVIA",
            "short_name": "LV",
            "iso_code\t": "LVA",
            "tel_prefix": "371",
            "status": "inactive"
        },
        {
            "id": 118,
            "name": "LEBANON",
            "short_name": "LB",
            "iso_code\t": "LBN",
            "tel_prefix": "961",
            "status": "inactive"
        },
        {
            "id": 119,
            "name": "LESOTHO",
            "short_name": "LS",
            "iso_code\t": "LSO",
            "tel_prefix": "266",
            "status": "inactive"
        },
        {
            "id": 120,
            "name": "LIBERIA",
            "short_name": "LR",
            "iso_code\t": "LBR",
            "tel_prefix": "231",
            "status": "inactive"
        },
        {
            "id": 121,
            "name": "LIBYAN ARAB JAMAHIRIYA",
            "short_name": "LY",
            "iso_code\t": "LBY",
            "tel_prefix": "218",
            "status": "inactive"
        },
        {
            "id": 122,
            "name": "LIECHTENSTEIN",
            "short_name": "LI",
            "iso_code\t": "LIE",
            "tel_prefix": "423",
            "status": "inactive"
        },
        {
            "id": 123,
            "name": "LITHUANIA",
            "short_name": "LT",
            "iso_code\t": "LTU",
            "tel_prefix": "370",
            "status": "inactive"
        },
        {
            "id": 124,
            "name": "LUXEMBOURG",
            "short_name": "LU",
            "iso_code\t": "LUX",
            "tel_prefix": "352",
            "status": "inactive"
        },
        {
            "id": 125,
            "name": "MACAO",
            "short_name": "MO",
            "iso_code\t": "MAC",
            "tel_prefix": "853",
            "status": "inactive"
        },
        {
            "id": 126,
            "name": "MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF",
            "short_name": "MK",
            "iso_code\t": "MKD",
            "tel_prefix": "389",
            "status": "inactive"
        },
        {
            "id": 127,
            "name": "MADAGASCAR",
            "short_name": "MG",
            "iso_code\t": "MDG",
            "tel_prefix": "261",
            "status": "inactive"
        },
        {
            "id": 128,
            "name": "MALAWI",
            "short_name": "MW",
            "iso_code\t": "MWI",
            "tel_prefix": "265",
            "status": "inactive"
        },
        {
            "id": 129,
            "name": "MALAYSIA",
            "short_name": "MY",
            "iso_code\t": "MYS",
            "tel_prefix": "60",
            "status": "inactive"
        },
        {
            "id": 130,
            "name": "MALDIVES",
            "short_name": "MV",
            "iso_code\t": "MDV",
            "tel_prefix": "960",
            "status": "inactive"
        },
        {
            "id": 131,
            "name": "MALI",
            "short_name": "ML",
            "iso_code\t": "MLI",
            "tel_prefix": "223",
            "status": "inactive"
        },
        {
            "id": 132,
            "name": "MALTA",
            "short_name": "MT",
            "iso_code\t": "MLT",
            "tel_prefix": "356",
            "status": "inactive"
        },
        {
            "id": 133,
            "name": "MARSHALL ISLANDS",
            "short_name": "MH",
            "iso_code\t": "MHL",
            "tel_prefix": "692",
            "status": "inactive"
        },
        {
            "id": 134,
            "name": "MARTINIQUE",
            "short_name": "MQ",
            "iso_code\t": "MTQ",
            "tel_prefix": "596",
            "status": "inactive"
        },
        {
            "id": 135,
            "name": "MAURITANIA",
            "short_name": "MR",
            "iso_code\t": "MRT",
            "tel_prefix": "222",
            "status": "inactive"
        },
        {
            "id": 136,
            "name": "MAURITIUS",
            "short_name": "MU",
            "iso_code\t": "MUS",
            "tel_prefix": "230",
            "status": "inactive"
        },
        {
            "id": 137,
            "name": "MAYOTTE",
            "short_name": "YT",
            "iso_code\t": "YT",
            "tel_prefix": "269",
            "status": "inactive"
        },
        {
            "id": 138,
            "name": "MEXICO",
            "short_name": "MX",
            "iso_code\t": "MEX",
            "tel_prefix": "52",
            "status": "inactive"
        },
        {
            "id": 139,
            "name": "MICRONESIA, FEDERATED STATES OF",
            "short_name": "FM",
            "iso_code\t": "FSM",
            "tel_prefix": "691",
            "status": "inactive"
        },
        {
            "id": 140,
            "name": "MOLDOVA, REPUBLIC OF",
            "short_name": "MD",
            "iso_code\t": "MDA",
            "tel_prefix": "373",
            "status": "inactive"
        },
        {
            "id": 141,
            "name": "MONACO",
            "short_name": "MC",
            "iso_code\t": "MCO",
            "tel_prefix": "377",
            "status": "inactive"
        },
        {
            "id": 142,
            "name": "MONGOLIA",
            "short_name": "MN",
            "iso_code\t": "MNG",
            "tel_prefix": "976",
            "status": "inactive"
        },
        {
            "id": 143,
            "name": "MONTSERRAT",
            "short_name": "MS",
            "iso_code\t": "MSR",
            "tel_prefix": "1664",
            "status": "inactive"
        },
        {
            "id": 144,
            "name": "MOROCCO",
            "short_name": "MA",
            "iso_code\t": "MAR",
            "tel_prefix": "212",
            "status": "inactive"
        },
        {
            "id": 145,
            "name": "MOZAMBIQUE",
            "short_name": "MZ",
            "iso_code\t": "MOZ",
            "tel_prefix": "258",
            "status": "inactive"
        },
        {
            "id": 146,
            "name": "MYANMAR",
            "short_name": "MM",
            "iso_code\t": "MMR",
            "tel_prefix": "95",
            "status": "inactive"
        },
        {
            "id": 147,
            "name": "NAMIBIA",
            "short_name": "NA",
            "iso_code\t": "NAM",
            "tel_prefix": "264",
            "status": "inactive"
        },
        {
            "id": 148,
            "name": "NAURU",
            "short_name": "NR",
            "iso_code\t": "NRU",
            "tel_prefix": "674",
            "status": "inactive"
        },
        {
            "id": 149,
            "name": "NEPAL",
            "short_name": "NP",
            "iso_code\t": "NPL",
            "tel_prefix": "977",
            "status": "inactive"
        },
        {
            "id": 150,
            "name": "NETHERLANDS",
            "short_name": "NL",
            "iso_code\t": "NLD",
            "tel_prefix": "31",
            "status": "inactive"
        },
        {
            "id": 151,
            "name": "NETHERLANDS ANTILLES",
            "short_name": "AN",
            "iso_code\t": "ANT",
            "tel_prefix": "599",
            "status": "inactive"
        },
        {
            "id": 152,
            "name": "NEW CALEDONIA",
            "short_name": "NC",
            "iso_code\t": "NCL",
            "tel_prefix": "687",
            "status": "inactive"
        },
        {
            "id": 153,
            "name": "NEW ZEALAND",
            "short_name": "NZ",
            "iso_code\t": "NZL",
            "tel_prefix": "64",
            "status": "inactive"
        },
        {
            "id": 154,
            "name": "NICARAGUA",
            "short_name": "NI",
            "iso_code\t": "NIC",
            "tel_prefix": "505",
            "status": "inactive"
        },
        {
            "id": 155,
            "name": "NIGER",
            "short_name": "NE",
            "iso_code\t": "NER",
            "tel_prefix": "227",
            "status": "inactive"
        },
        {
            "id": 156,
            "name": "NIGERIA",
            "short_name": "NG",
            "iso_code\t": "NGA",
            "tel_prefix": "234",
            "status": "inactive"
        },
        {
            "id": 157,
            "name": "NIUE",
            "short_name": "NU",
            "iso_code\t": "NIU",
            "tel_prefix": "683",
            "status": "inactive"
        },
        {
            "id": 158,
            "name": "NORFOLK ISLAND",
            "short_name": "NF",
            "iso_code\t": "NFK",
            "tel_prefix": "672",
            "status": "inactive"
        },
        {
            "id": 159,
            "name": "NORTHERN MARIANA ISLANDS",
            "short_name": "MP",
            "iso_code\t": "MNP",
            "tel_prefix": "1670",
            "status": "inactive"
        },
        {
            "id": 160,
            "name": "NORWAY",
            "short_name": "NO",
            "iso_code\t": "NOR",
            "tel_prefix": "47",
            "status": "inactive"
        },
        {
            "id": 161,
            "name": "OMAN",
            "short_name": "OM",
            "iso_code\t": "OMN",
            "tel_prefix": "968",
            "status": "inactive"
        },
        {
            "id": 162,
            "name": "PAKISTAN",
            "short_name": "PK",
            "iso_code\t": "PAK",
            "tel_prefix": "92",
            "status": "inactive"
        },
        {
            "id": 163,
            "name": "PALAU",
            "short_name": "PW",
            "iso_code\t": "PLW",
            "tel_prefix": "680",
            "status": "inactive"
        },
        {
            "id": 164,
            "name": "PALESTINIAN TERRITORY, OCCUPIED",
            "short_name": "PS",
            "iso_code\t": "Ps",
            "tel_prefix": "970",
            "status": "inactive"
        },
        {
            "id": 165,
            "name": "PANAMA",
            "short_name": "PA",
            "iso_code\t": "PAN",
            "tel_prefix": "507",
            "status": "inactive"
        },
        {
            "id": 166,
            "name": "PAPUA NEW GUINEA",
            "short_name": "PG",
            "iso_code\t": "PNG",
            "tel_prefix": "675",
            "status": "inactive"
        },
        {
            "id": 167,
            "name": "PARAGUAY",
            "short_name": "PY",
            "iso_code\t": "PRY",
            "tel_prefix": "595",
            "status": "inactive"
        },
        {
            "id": 168,
            "name": "PERU",
            "short_name": "PE",
            "iso_code\t": "PER",
            "tel_prefix": "51",
            "status": "inactive"
        },
        {
            "id": 169,
            "name": "PHILIPPINES",
            "short_name": "PH",
            "iso_code\t": "PHL",
            "tel_prefix": "63",
            "status": "inactive"
        },
        {
            "id": 170,
            "name": "PITCAIRN",
            "short_name": "PN",
            "iso_code\t": "PCN",
            "tel_prefix": "11",
            "status": "inactive"
        },
        {
            "id": 171,
            "name": "POLAND",
            "short_name": "PL",
            "iso_code\t": "POL",
            "tel_prefix": "48",
            "status": "inactive"
        },
        {
            "id": 172,
            "name": "PORTUGAL",
            "short_name": "PT",
            "iso_code\t": "PRT",
            "tel_prefix": "351",
            "status": "inactive"
        },
        {
            "id": 173,
            "name": "PUERTO RICO",
            "short_name": "PR",
            "iso_code\t": "PRI",
            "tel_prefix": "1787",
            "status": "inactive"
        },
        {
            "id": 174,
            "name": "QATAR",
            "short_name": "QA",
            "iso_code\t": "QAT",
            "tel_prefix": "974",
            "status": "inactive"
        },
        {
            "id": 175,
            "name": "REUNION",
            "short_name": "RE",
            "iso_code\t": "REU",
            "tel_prefix": "262",
            "status": "inactive"
        },
        {
            "id": 176,
            "name": "ROMANIA",
            "short_name": "RO",
            "iso_code\t": "ROM",
            "tel_prefix": "40",
            "status": "inactive"
        },
        {
            "id": 177,
            "name": "RUSSIAN FEDERATION",
            "short_name": "RU",
            "iso_code\t": "RUS",
            "tel_prefix": "70",
            "status": "inactive"
        },
        {
            "id": 178,
            "name": "RWANDA",
            "short_name": "RW",
            "iso_code\t": "RWA",
            "tel_prefix": "250",
            "status": "inactive"
        },
        {
            "id": 179,
            "name": "SAINT HELENA",
            "short_name": "SH",
            "iso_code\t": "SHN",
            "tel_prefix": "290",
            "status": "inactive"
        },
        {
            "id": 180,
            "name": "SAINT KITTS AND NEVIS",
            "short_name": "KN",
            "iso_code\t": "KNA",
            "tel_prefix": "1869",
            "status": "inactive"
        },
        {
            "id": 181,
            "name": "SAINT LUCIA",
            "short_name": "LC",
            "iso_code\t": "LCA",
            "tel_prefix": "1758",
            "status": "inactive"
        },
        {
            "id": 182,
            "name": "SAINT PIERRE AND MIQUELON",
            "short_name": "PM",
            "iso_code\t": "SPM",
            "tel_prefix": "508",
            "status": "inactive"
        },
        {
            "id": 183,
            "name": "SAINT VINCENT AND THE GRENADINES",
            "short_name": "VC",
            "iso_code\t": "VCT",
            "tel_prefix": "1784",
            "status": "inactive"
        },
        {
            "id": 184,
            "name": "SAMOA",
            "short_name": "WS",
            "iso_code\t": "WSM",
            "tel_prefix": "684",
            "status": "inactive"
        },
        {
            "id": 185,
            "name": "SAN MARINO",
            "short_name": "SM",
            "iso_code\t": "SMR",
            "tel_prefix": "378",
            "status": "inactive"
        },
        {
            "id": 186,
            "name": "SAO TOME AND PRINCIPE",
            "short_name": "ST",
            "iso_code\t": "STP",
            "tel_prefix": "239",
            "status": "inactive"
        },
        {
            "id": 187,
            "name": "SAUDI ARABIA",
            "short_name": "SA",
            "iso_code\t": "SAU",
            "tel_prefix": "966",
            "status": "inactive"
        },
        {
            "id": 188,
            "name": "SENEGAL",
            "short_name": "SN",
            "iso_code\t": "SEN",
            "tel_prefix": "221",
            "status": "inactive"
        },
        {
            "id": 189,
            "name": "SERBIA AND MONTENEGRO",
            "short_name": "CS",
            "iso_code\t": "CS",
            "tel_prefix": "381",
            "status": "inactive"
        },
        {
            "id": 190,
            "name": "SEYCHELLES",
            "short_name": "SC",
            "iso_code\t": "SYC",
            "tel_prefix": "248",
            "status": "inactive"
        },
        {
            "id": 191,
            "name": "SIERRA LEONE",
            "short_name": "SL",
            "iso_code\t": "SLE",
            "tel_prefix": "232",
            "status": "inactive"
        },
        {
            "id": 192,
            "name": "SINGAPORE",
            "short_name": "SG",
            "iso_code\t": "SGP",
            "tel_prefix": "65",
            "status": "inactive"
        },
        {
            "id": 193,
            "name": "SLOVAKIA",
            "short_name": "SK",
            "iso_code\t": "SVK",
            "tel_prefix": "421",
            "status": "inactive"
        },
        {
            "id": 194,
            "name": "SLOVENIA",
            "short_name": "SI",
            "iso_code\t": "SVN",
            "tel_prefix": "386",
            "status": "inactive"
        },
        {
            "id": 195,
            "name": "SOLOMON ISLANDS",
            "short_name": "SB",
            "iso_code\t": "SLB",
            "tel_prefix": "677",
            "status": "inactive"
        },
        {
            "id": 196,
            "name": "SOMALIA",
            "short_name": "SO",
            "iso_code\t": "SOM",
            "tel_prefix": "252",
            "status": "inactive"
        },
        {
            "id": 197,
            "name": "SOUTH AFRICA",
            "short_name": "ZA",
            "iso_code\t": "ZAF",
            "tel_prefix": "27",
            "status": "inactive"
        },
        {
            "id": 198,
            "name": "SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS",
            "short_name": "GS",
            "iso_code\t": "GS",
            "tel_prefix": "11",
            "status": "inactive"
        },
        {
            "id": 199,
            "name": "SPAIN",
            "short_name": "ES",
            "iso_code\t": "ESP",
            "tel_prefix": "34",
            "status": "inactive"
        },
        {
            "id": 200,
            "name": "SRI LANKA",
            "short_name": "LK",
            "iso_code\t": "LKA",
            "tel_prefix": "94",
            "status": "inactive"
        },
        {
            "id": 201,
            "name": "SUDAN",
            "short_name": "SD",
            "iso_code\t": "SDN",
            "tel_prefix": "249",
            "status": "inactive"
        },
        {
            "id": 202,
            "name": "SURINAME",
            "short_name": "SR",
            "iso_code\t": "SUR",
            "tel_prefix": "597",
            "status": "inactive"
        },
        {
            "id": 203,
            "name": "SVALBARD AND JAN MAYEN",
            "short_name": "SJ",
            "iso_code\t": "SJM",
            "tel_prefix": "47",
            "status": "inactive"
        },
        {
            "id": 204,
            "name": "SWAZILAND",
            "short_name": "SZ",
            "iso_code\t": "SWZ",
            "tel_prefix": "268",
            "status": "inactive"
        },
        {
            "id": 205,
            "name": "SWEDEN",
            "short_name": "SE",
            "iso_code\t": "SWE",
            "tel_prefix": "46",
            "status": "inactive"
        },
        {
            "id": 206,
            "name": "SWITZERLAND",
            "short_name": "CH",
            "iso_code\t": "CHE",
            "tel_prefix": "41",
            "status": "inactive"
        },
        {
            "id": 207,
            "name": "SYRIAN ARAB REPUBLIC",
            "short_name": "SY",
            "iso_code\t": "SYR",
            "tel_prefix": "963",
            "status": "inactive"
        },
        {
            "id": 208,
            "name": "TAIWAN, PROVINCE OF CHINA",
            "short_name": "TW",
            "iso_code\t": "TWN",
            "tel_prefix": "886",
            "status": "inactive"
        },
        {
            "id": 209,
            "name": "TAJIKISTAN",
            "short_name": "TJ",
            "iso_code\t": "TJK",
            "tel_prefix": "992",
            "status": "inactive"
        },
        {
            "id": 210,
            "name": "TANZANIA, UNITED REPUBLIC OF",
            "short_name": "TZ",
            "iso_code\t": "TZA",
            "tel_prefix": "255",
            "status": "inactive"
        },
        {
            "id": 211,
            "name": "THAILAND",
            "short_name": "TH",
            "iso_code\t": "THA",
            "tel_prefix": "66",
            "status": "inactive"
        },
        {
            "id": 212,
            "name": "TIMOR-LESTE",
            "short_name": "TL",
            "iso_code\t": "TL",
            "tel_prefix": "670",
            "status": "inactive"
        },
        {
            "id": 213,
            "name": "TOGO",
            "short_name": "TG",
            "iso_code\t": "TGO",
            "tel_prefix": "228",
            "status": "inactive"
        },
        {
            "id": 214,
            "name": "TOKELAU",
            "short_name": "TK",
            "iso_code\t": "TKL",
            "tel_prefix": "690",
            "status": "inactive"
        },
        {
            "id": 215,
            "name": "TONGA",
            "short_name": "TO",
            "iso_code\t": "TON",
            "tel_prefix": "676",
            "status": "inactive"
        },
        {
            "id": 216,
            "name": "TRINIDAD AND TOBAGO",
            "short_name": "TT",
            "iso_code\t": "TTO",
            "tel_prefix": "1868",
            "status": "inactive"
        },
        {
            "id": 217,
            "name": "TUNISIA",
            "short_name": "TN",
            "iso_code\t": "TUN",
            "tel_prefix": "216",
            "status": "inactive"
        },
        {
            "id": 218,
            "name": "TURKEY",
            "short_name": "TR",
            "iso_code\t": "TUR",
            "tel_prefix": "90",
            "status": "inactive"
        },
        {
            "id": 219,
            "name": "TURKMENISTAN",
            "short_name": "TM",
            "iso_code\t": "TKM",
            "tel_prefix": "7370",
            "status": "inactive"
        },
        {
            "id": 220,
            "name": "TURKS AND CAICOS ISLANDS",
            "short_name": "TC",
            "iso_code\t": "TCA",
            "tel_prefix": "1649",
            "status": "inactive"
        },
        {
            "id": 221,
            "name": "TUVALU",
            "short_name": "TV",
            "iso_code\t": "TUV",
            "tel_prefix": "688",
            "status": "inactive"
        },
        {
            "id": 222,
            "name": "UGANDA",
            "short_name": "UG",
            "iso_code\t": "UGA",
            "tel_prefix": "256",
            "status": "inactive"
        },
        {
            "id": 223,
            "name": "UKRAINE",
            "short_name": "UA",
            "iso_code\t": "UKR",
            "tel_prefix": "380",
            "status": "inactive"
        },
        {
            "id": 224,
            "name": "UNITED ARAB EMIRATES",
            "short_name": "AE",
            "iso_code\t": "ARE",
            "tel_prefix": "971",
            "status": "inactive"
        },
        {
            "id": 225,
            "name": "UNITED KINGDOM",
            "short_name": "GB",
            "iso_code\t": "GBR",
            "tel_prefix": "44",
            "status": "inactive"
        },
        {
            "id": 226,
            "name": "UNITED STATES",
            "short_name": "US",
            "iso_code\t": "USA",
            "tel_prefix": "1",
            "status": "inactive"
        },
        {
            "id": 227,
            "name": "UNITED STATES MINOR OUTLYING ISLANDS",
            "short_name": "UM",
            "iso_code\t": "UM",
            "tel_prefix": "1",
            "status": "inactive"
        },
        {
            "id": 228,
            "name": "URUGUAY",
            "short_name": "UY",
            "iso_code\t": "URY",
            "tel_prefix": "598",
            "status": "inactive"
        },
        {
            "id": 229,
            "name": "UZBEKISTAN",
            "short_name": "UZ",
            "iso_code\t": "UZB",
            "tel_prefix": "998",
            "status": "inactive"
        },
        {
            "id": 230,
            "name": "VANUATU",
            "short_name": "VU",
            "iso_code\t": "VUT",
            "tel_prefix": "678",
            "status": "inactive"
        },
        {
            "id": 231,
            "name": "VENEZUELA",
            "short_name": "VE",
            "iso_code\t": "VEN",
            "tel_prefix": "58",
            "status": "inactive"
        },
        {
            "id": 232,
            "name": "VIET NAM",
            "short_name": "VN",
            "iso_code\t": "VNM",
            "tel_prefix": "84",
            "status": "inactive"
        },
        {
            "id": 233,
            "name": "VIRGIN ISLANDS, BRITISH",
            "short_name": "VG",
            "iso_code\t": "VGB",
            "tel_prefix": "1284",
            "status": "inactive"
        },
        {
            "id": 234,
            "name": "VIRGIN ISLANDS, U.S.",
            "short_name": "VI",
            "iso_code\t": "VIR",
            "tel_prefix": "1340",
            "status": "inactive"
        },
        {
            "id": 235,
            "name": "WALLIS AND FUTUNA",
            "short_name": "WF",
            "iso_code\t": "WLF",
            "tel_prefix": "681",
            "status": "inactive"
        },
        {
            "id": 236,
            "name": "WESTERN SAHARA",
            "short_name": "EH",
            "iso_code\t": "ESH",
            "tel_prefix": "212",
            "status": "inactive"
        },
        {
            "id": 237,
            "name": "YEMEN",
            "short_name": "YE",
            "iso_code\t": "YEM",
            "tel_prefix": "967",
            "status": "inactive"
        },
        {
            "id": 238,
            "name": "ZAMBIA",
            "short_name": "ZM",
            "iso_code\t": "ZMB",
            "tel_prefix": "260",
            "status": "inactive"
        },
        {
            "id": 239,
            "name": "ZIMBABWE",
            "short_name": "ZW",
            "iso_code\t": "ZWE",
            "tel_prefix": "263",
            "status": "inactive"
        }
    ];

    let factory = getFactory();

    const countryRegistry = await getAssetRegistry('org.axir.chain.Country');
    const countryResources = [];
    country.forEach(function (cun) {
        // console.log(cun);
        const addcountry = factory.newResource('org.axir.chain', 'Country', cun.short_name);

        addcountry.countryFullName = cun.name;
        addcountry.countryDialCode = cun.tel_prefix;
        addcountry.countryIsoCode = cun.iso_code
        countryResources.push(addcountry);

    });
    //console.log(countryResources);
    await countryRegistry.addAll(countryResources);
}