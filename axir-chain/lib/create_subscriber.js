/**
 * Create Subscriber.
 * @param {org.axir.chain.CreateSubscriber} subscriber
 * @transaction
 */

async function createSubscriber(subscriber) {

    // Get the subscriber partipant registry
    await getParticipantRegistry('org.axir.chain.Subscriber')
        .then(function (subscriberParticipantRegistry) {
            // Get the factory for creating new asset instances.
            let factory = getFactory();
            
            //@todo do the exist valiation and throw error - subscriber with email already exists in axir-chain
            let newWalletAddress = "Subscriber Wallet for " + subscriber.firstName;
            let newWalletLabel = "Wallet Label for " + subscriber.firstName;

            // Create the new subscriber.
            let newSubscriber = factory.newResource('org.axir.chain', 'Subscriber', subscriber.email);
          
            newSubscriber.firstName = subscriber.firstName;
            newSubscriber.lastName = subscriber.lastName;

            let newSubscriberWallet = factory.newRelationship('org.axir.chain', 'Wallet', newWalletAddress);
            newSubscriberWallet.walletLabel = newWalletLabel;
            

            // Add the subscriber to the Registry.
            subscriberParticipantRegistry.add(newSubscriber);

            // emit a notification that a subscriber has created
            let newSubscriberNotification = getFactory().newEvent('org.axir.chain', 'NewSubscriberCreated');
            newSubscriberNotification.email = subscriber.email;
            newSubscriberNotification.firstName = subscriber.firstName;
            newSubscriberNotification.lastName = subscriber.lastName;
            emit(newSubscriberNotification);

        })
        .catch(function (error) {
            console.log(error);
        });
}