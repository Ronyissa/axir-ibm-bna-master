/**
 * Seed Mobile Operators .
 * @param {org.axir.chain.SeedMobileOperators} null
 * @transaction
 */

async function seedMobileOperators() {

    await createMobileOperator(
        {
            "shortName": "Airtel",
            "fullName": "Airtel India Pvt Ltd",
            "email": "admin@airtel.in",
            "countryShortCode": "IN"
        }
    )

    await createWallet(
        {
            "newWalletAddress": "Airtel-Master-Wallet",
            "newWalletLabel": "Wallet created for Airtel Master Admin"
        }
    )
    await createWallet(
        {
            "newWalletAddress": "Airtel-Revenue-Wallet",
            "newWalletLabel": "Wallet created for Collecting revenues"
        }
    )

    // Vodofone 

    await createMobileOperator(
        {
            "shortName": "Vodofone",
            "fullName": "Vodofone India Pvt Ltd",
            "email": "admin@vodofone.in",
            "countryShortCode": "IN"
        }
    )

    await createWallet(
        {
            "newWalletAddress": "Vodofone-Master-Wallet",
            "newWalletLabel": "Wallet created for Vodofone Master Admin"
        }
    )
    await createWallet(
        {
            "newWalletAddress": "Vodofone-Revenue-Wallet",
            "newWalletLabel": "Wallet created for Collecting revenues"
        }
    )
    
     // Verizon

    await createMobileOperator(
        {
            "shortName": "Verizon",
            "fullName": "Verizon Ltd",
            "email": "admin@verizon.us",
            "countryShortCode": "US"
        }
    )

    await createWallet(
        {
            "newWalletAddress": "Verizon-Master-Wallet",
            "newWalletLabel": "Wallet created for Verizon Master Admin"
        }
    )
    await createWallet(
        {
            "newWalletAddress": "Verizon-Revenue-Wallet",
            "newWalletLabel": "Wallet created for Collecting revenues"
        }
    )

    // Etisalat

    await createMobileOperator(
        {
            "shortName": "Etisalat",
            "fullName": "Etisalat Ltd",
            "email": "admin@etisalat.us",
            "countryShortCode": "AE"
        }
    )

    await createWallet(
        {
            "newWalletAddress": "Etisalat-Master-Wallet",
            "newWalletLabel": "Wallet created for Etisalat Master Admin"
        }
    )
    await createWallet(
        {
            "newWalletAddress": "Etisalat-Revenue-Wallet",
            "newWalletLabel": "Wallet created for Collecting revenues"
        }
    )

}