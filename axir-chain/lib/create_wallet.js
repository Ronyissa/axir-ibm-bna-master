/**
 * Create Wallet.
 * @param {org.axir.chain.CreateWallet} wt
 * @transaction
 */


async function createWallet(wt) {


    //@todo Create etherum Address using any JS library, so we can only need label to create the address. For time being use any online tool to create the Ethereum address like MyEtherWallet and use that address.

    //@todo Vaidation for checking wallet exists

    await getAssetRegistry('org.axir.chain' + '.Wallet')
        .then(function (walletRegistry) {
            let newWallet = getFactory().newResource('org.axir.chain', 'Wallet', wt.newWalletAddress);

            newWallet.walletLabel = wt.newWalletLabel;
            return walletRegistry.add(newWallet);
        })
        .catch(function (error) {
            console.log(error);
        });
}