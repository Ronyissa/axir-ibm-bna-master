/**
 * Create Mobile Operator.
 * @param {org.axir.chain.CreateMobileOperator} mo
 * @transaction
 */

async function createMobileOperator(mo) {

    let operatorCountry = await getAssetRegistry('org.axir.chain.Country')
        .then((registryData) => registryData.get(mo.countryShortCode));


    // Get the Mobile Operator partipant registry
    await getParticipantRegistry('org.axir.chain.MobileOperator')
        .then(function (moRegistry) {


            // Get the factory for creating new asset instances.
            let factory = getFactory();

            // Create the new Mobile Operator
            let newOperator = factory.newResource('org.axir.chain', 'MobileOperator', mo.email);

            newOperator.operatorShortName = mo.shortName;
            newOperator.operatorFullName = mo.fullName;
            newOperator.country = operatorCountry;

            // Add the Mobile Operator to the Registry.
            moRegistry.add(newOperator);

            // emit a notification that a Mobile Operator has created
            let newNotification = getFactory().newEvent('org.axir.chain', 'NewMobileOperatorCreated');
            newNotification.shortName = mo.shortName;
            newNotification.fullName = mo.fullName;
            newNotification.email = mo.email;

            emit(newNotification);

        })
        .catch(function (error) {
            console.log(error);
        }
        );
}