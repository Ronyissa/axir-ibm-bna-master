/**
 * Create Axir Admin User.
 * @param {org.axir.chain.CreateAxirAdmin} nu
 * @transaction
 */


async function createAxirAdmin(nu) {

    //@todo Validate User Exists and throw Error

    await getParticipantRegistry('org.axir.chain.AxirAdmin')
        .then(function (registryData) {
            let newUser = getFactory().newResource('org.axir.chain', 'AxirAdmin', nu.email);

            //@todo Create an Event NewUserCreated

            return registryData.add(newUser);
        })
        .catch(function (error) {
            console.log(error);
        });
}