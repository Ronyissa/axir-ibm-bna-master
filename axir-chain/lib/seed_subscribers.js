/**
 * Seed Subscribers.
 * @param {org.axir.chain.SeedSubscribers} tx
 * @transaction
 */

async function seedSubscribers(tx) {

    let owner = getCurrentParticipant()
    let count = await query('selectAllSubscribers')
    
    console.log(count);
    console.log(currentUser);
    console.log(tx.numberOfSubscribers)
    console.log(count+tx.numberOfSubscribers)


    for (var i = 1; i <= tx.numberOfSubscribers; i++) {
        await createSubscriber({
            'email': owner.operatorShortName + 'subscriber' + i + '@axirmail.com',
            'firstName': owner.operatorShortName + 'subscriberfirst' + i,
            'lastName': operatorShortName + 'subscriberlast' + i,
            'created_by': owner
        })
        await createWallet(
            {
                "email": owner.operatorShortName + 'subscriber' + i + '@axirmail.com',
                "newWalletAddress": owner.operatorShortName + 'subscriber' + i + "-Wallet",
                "newWalletLabel": "Auto created wallet for " + 'subscriber' + i + '@axirmail.com'
            }
        )
    }
}