/**
 * Seed Axir .
 * @param {org.axir.chain.SeedAxir} null
 * @transaction
 */

async function seedAxir() {

    //create an axir admin user
    await createAxirAdmin({ "email": "masteradmin@axir.io"})
    
    await createWallet(
        {
            "newWalletAddress":"Master-Axir-Wallet", 
            "newWalletLabel":"Wallet created for Axir Master Admin" 
        }
    )
    await createWallet(
        {
            "newWalletAddress": "Master-Fee-Wallet",
            "newWalletLabel": "Wallet created for Collecting Fees"
        }
    )

}

