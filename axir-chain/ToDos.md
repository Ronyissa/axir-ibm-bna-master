Todos
===
1. Define country as Asset
2. Create Country Seeder - to populate the data
3. Define Abstract Class User 
4. Define Class Wallet
5. Extend User class to AxirAdmin
6. Create a Transaction Processor to seed AxirAdmin and create Master Wallet & Fee Wallet
7. Extend User class to MobileOperator
8. Extend User class to VirtualMobileOperator
9. Create a Transaction Processor to create MobileOperator
10. Create a Transaction Processor to seed MobileOperator
11. Create a Transaction Processor to CreditWallet
12. Create a Tranaction Processor to DebitWallet
13. Write a Function to getFeeWallet from the Asset Registry
14. Write a Function to estimateFee based on NetworkBaseFee + FeeVariant 
15. Create a Transaction Processor to TransferFund from one Wallet to another Wallet
16. Create a Transaction Processor to SendMany - sending fund from One Wallet to Many Wallet
17. Create a Transaction Processor to CreateMobileOperator
18. Create a Transaction Processor to CreateSubscriber
19. Create a Transaction Processor to SeedMobileOperator
20. Create a Transaction Processor to SeedSubscriber
21. Create a Transaction Processor to SeedSubscriber by specifiying numbers
22. Define Purchaseable Abstract Class
23. Define Contract

